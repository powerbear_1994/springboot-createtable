package org.spring.springboot.util;

import org.spring.springboot.annotation.TableTag;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Properties;

/**
 * @Author: powerbear
 * @Date: 2019/4/23 14:39
 * @Version 1.0
 */

public class ColumnUtil {
    public static final String POT = ".";

    private static Properties properties = new Properties();

    public static String getColumnName(String column) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < column.length(); i++) {
            char c = column.charAt(i);
            if (!Character.isLowerCase(c)) {
                sb.append("_").append(String.valueOf(c).toLowerCase());
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String getTableName(String tableName) {
        if (tableName.contains(POT)) {
            String[] tables = tableName.split("\\.");
            tableName = tables[tables.length - 1];
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < tableName.length(); i++) {
            char c = tableName.charAt(i);
            if (!Character.isLowerCase(c)) {
                if (i == 0) {
                    sb.append(String.valueOf(c).toLowerCase());
                } else {
                    sb.append("_").append(String.valueOf(c).toLowerCase());
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String getCreateSql(Object obj) {
        Class<?> clz = obj.getClass();
        Field[] fields = clz.getDeclaredFields();
        StringBuffer sql = new StringBuffer("CREATE TABLE ");
        String tableName = getTableName(clz.getName());
        sql.append(tableName).append(" (");
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            Annotation[] aArray = field.getDeclaredAnnotations();
            boolean isPK = false;
            String length = "0";
            if (aArray != null) {
                for (Annotation an : aArray) {
                    TableTag tag = (TableTag) an;
                    isPK = tag.isPK();
                    length = tag.length();
                }
            }
            String f = field.getName();
            String column = ColumnUtil.getColumnName(f);
            try {
                InputStream is = ColumnUtil.class.getClass().getResourceAsStream("/type.properties");
                properties.load(is);
                String fileType = field.getType().toString();
                String[] fileTypes = fileType.split("\\.");
                fileType = (String) properties.get(fileTypes[fileTypes.length - 1]);
                sql.append(column).append(" ").append(fileType);
                if (!(length.equals("0"))) {
                    sql.append("(").append(length).append(")");
                }
                if (isPK) {
                    sql.append(" not null primary key ");
                }
                if (i == fields.length - 1) {
                    sql.append(" )");
                } else {
                    sql.append(" ,");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.err.println(sql.toString());
        return sql.toString();
    }

}
