package org.spring.springboot;

import org.spring.springboot.model.Person;
import org.spring.springboot.util.ColumnUtil;
import org.spring.springboot.util.DBHelper;

import java.sql.SQLException;

/**
 * @Author: powerbear
 * @Date: 2019/4/23 14:39
 * @Version 1.0
 */

public class Main {

    static DBHelper db1 = null;

    public static void main(String[] args) {
        String sql = ColumnUtil.getCreateSql(new Person());
        db1 = new DBHelper(sql);
        try {
            db1.pst.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
