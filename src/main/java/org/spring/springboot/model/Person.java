package org.spring.springboot.model;

import org.spring.springboot.annotation.TableTag;

import java.util.Date;

/**
 * @Author: powerbear
 * @Date: 2019/4/23 14:39
 * @Version 1.0
 */
public class Person {
    @TableTag(isPK = true, length = "11")
    private Integer personId;
    @TableTag(length = "2")
    private Integer age;
    @TableTag(length = "50")
    private String name;
    @TableTag(length = "50")
    private String nickName;
    @TableTag()
    private Date birthday;
    @TableTag(length = "1")
    private Boolean isAdmin;
    @TableTag(length = "11,2")
    private Double wealth;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Double getWealth() {
        return wealth;
    }

    public void setWealth(Double wealth) {
        this.wealth = wealth;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                ", birthday=" + birthday +
                ", isAdmin=" + isAdmin +
                ", wealth=" + wealth +
                '}';
    }
}

