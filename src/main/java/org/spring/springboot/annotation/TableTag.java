package org.spring.springboot.annotation;

import java.lang.annotation.*;

/**
 * @Author: powerbear
 * @Date: 2019/4/23 14:34
 * @Version 1.0
 */

/**
 * 自定义注解详细解释见 https://www.cnblogs.com/liangweiping/p/3837332.html
 */

@Documented
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TableTag {
    /**
     * 是否是主键
     * @return
     */
    boolean isPK() default false;

    /**
     * 字段的长度
     * @return
     */
    String length() default "0";
}
